package de.richtercloud.hibernate.foreign.key.constraint.violation;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Entity3 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne
    private Entity0 entity0;

    public Entity3() {
        super();
    }
    public Entity3(Entity0 user) {
        super();
        this.entity0 = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Entity0 getEntity0() {
        return entity0;
    }

    public void setEntity0(Entity0 entity0) {
        this.entity0 = entity0;
    }
}
