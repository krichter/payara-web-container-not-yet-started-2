package de.richtercloud.hibernate.foreign.key.constraint.violation;

import java.io.Serializable;
import javax.ejb.Local;

@Local
public interface EnterpriseBean0I extends Serializable {

    void method0(Entity2 entity2,
            Entity0 entity0);

    void method1(Entity2 entity2,
            Entity0 entity0);
}
