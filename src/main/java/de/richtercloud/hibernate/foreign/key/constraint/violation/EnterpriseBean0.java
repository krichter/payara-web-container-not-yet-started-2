package de.richtercloud.hibernate.foreign.key.constraint.violation;

import com.google.common.collect.MoreCollectors;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless
public class EnterpriseBean0 implements EnterpriseBean0I {
    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LoggerFactory.getLogger(EnterpriseBean0.class);
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void method0(Entity2 entity2,
            Entity0 entity0) {
        entityManager.persist(entity0);
        LOGGER.trace("entity0.id: {}",
                entity0.getId());
        if(entity2.getEntity3s().stream()
                .anyMatch(entity3 -> entity3.getEntity0().getId().equals(entity0.getId()))) {
            throw new IllegalArgumentException();
        }
        LOGGER.trace(String.format("adding entity0 with id %d",
                entity0.getId()));
        Entity3 entity3 = new Entity3(entity0);
        entityManager.persist(entity3);
        entity2.getEntity3s().add(entity3);
        entityManager.merge(entity2);
    }
    
    @Override
    public void method1(Entity2 entity2,
            Entity0 entity0) {
        if(!entity2.getEntity3s().stream()
                .anyMatch(entity3 -> entity3.getEntity0().getId().equals(entity0.getId()))) {
            throw new IllegalArgumentException();
        }
        Entity3 toRemove = entity2.getEntity3s().stream()
                .filter(entity3 -> entity3.getEntity0().getId().equals(entity0.getId()))
                .collect(MoreCollectors.onlyElement());
        LOGGER.trace(String.format("entity2.entity3s.size: %d",
                entity2.getEntity3s().size()));
        entityManager.remove(entityManager.merge(toRemove));
        entity2.getEntity3s().removeIf(entity3 -> entity3.getEntity0().getId().equals(entity0.getId()));
        LOGGER.trace(String.format("entity2.entity3s.size: %d",
                entity2.getEntity3s().size()));
        entityManager.merge(entity2);
    }
}
