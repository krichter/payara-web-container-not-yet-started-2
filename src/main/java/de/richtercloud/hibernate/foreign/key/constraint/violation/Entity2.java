package de.richtercloud.hibernate.foreign.key.constraint.violation;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.Valid;

@Entity
public class Entity2 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue
    private Long id;
    @OneToMany(fetch = FetchType.EAGER)
    @Valid
    private Set<Entity3> entity3s = new HashSet<>();

    public Entity2() {
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Entity3> getEntity3s() {
        return entity3s;
    }

    public void setEntity3s(Set<Entity3> entity3s) {
        this.entity3s = entity3s;
    }
}
